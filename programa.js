function calcularFactorial()
{
    var numero=document.getElementById('numeroIngresado').value;
    var resultado=1;
    for(var i=1; i<=numero; i++)
    {
        resultado=resultado*i;
    }
    document.getElementById('textoResultado').innerHTML=`El factorial del número ${numero} es: ${resultado}`;
}